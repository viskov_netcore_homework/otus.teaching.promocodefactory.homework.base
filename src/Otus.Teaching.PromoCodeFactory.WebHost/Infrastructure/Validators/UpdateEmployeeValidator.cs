﻿using FluentValidation;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Infrastructure.Validators
{
    public class UpdateEmployeeValidator: AbstractValidator<UpdateEmployeeCommand>
    {
        public UpdateEmployeeValidator()
        {
            RuleFor(x => x.Id)
                .NotNull()
                .WithMessage("Пользователь не указан");
            RuleFor(x => x.Email)
                .EmailAddress()
                .WithMessage("Не корректный емейл");
            RuleFor(x => x.FirstName)
                .NotEmpty()
                .WithMessage("Не указано имя");
            RuleFor(x => x.AppliedPromocodesCount)
                .GreaterThan(-1)
                .WithMessage("Неверное ко-во купонов");
            
        }
    }
}