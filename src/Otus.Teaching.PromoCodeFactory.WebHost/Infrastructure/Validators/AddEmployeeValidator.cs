﻿using FluentValidation;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Infrastructure.Validators
{
    public class AddEmployeeValidator: AbstractValidator<AddEmployeeCommand>
    {
        public AddEmployeeValidator()
        {
            RuleFor(x => x.Email)
                .EmailAddress()
                .WithMessage("Не корректный емейл");
            RuleFor(x => x.FirstName)
                .NotEmpty()
                .WithMessage("Не указано имя");
        }
    }
}