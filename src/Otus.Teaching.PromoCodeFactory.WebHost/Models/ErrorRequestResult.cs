﻿using System.Collections.Generic;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class ErrorRequestResult
    {
        public string[] Errors { get; set; }
        
        public ErrorRequestResult(IEnumerable<string> messages)
        {
            Errors = messages as string[] ?? messages.ToArray();
        }

        public ErrorRequestResult(string message)
        {
            Errors = new[] {message};
        }
    }
}