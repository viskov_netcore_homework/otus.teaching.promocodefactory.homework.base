﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class ApplicationSettings
    {
        public const string Position = "Settings";
        public int DefaultAppliedPromocodesCount { get; set; }
        public Guid[] DefaultRoles { get; set; }
    }
}