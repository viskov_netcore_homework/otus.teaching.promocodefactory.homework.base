﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Infrastructure.Extensions;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _rolesRepository;

        public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> rolesRepository)
        {
            _employeeRepository = employeeRepository;
            _rolesRepository = rolesRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }

        private EmployeeResponse CreateEmployeeResponse(Employee employee)
        {
            return  new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            return CreateEmployeeResponse(employee);
        }
        
        /// <summary>
        /// Добавить нового сотрудника
        /// </summary>
        /// <param name="command"><see cref="AddEmployeeCommand">DTO</see> нового сотрудника</param>
        /// <param name="settings">From Services</param>
        /// <param name="validator">From Services</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<EmployeeResponse>> AddEmployeeAsync(
            [FromBody] AddEmployeeCommand command, 
            [FromServices] IOptions<ApplicationSettings> settings,
            [FromServices] IValidator<AddEmployeeCommand> validator)
        {
            var validationResult = await validator.ValidateAsync(command);
            if (!validationResult.IsValid) 
                return BadRequest(new ErrorRequestResult(validationResult
                    .Errors
                    .Select(x => x.ErrorMessage)));
            var exist = await _employeeRepository.Where(x => x.Email == command.Email);
            if (exist.Any()) return BadRequest(new ErrorRequestResult("Пользователь с таким email уже существует"));
            var roles = command.Roles ?? settings.Value.DefaultRoles;
            var employee = new Employee
            {
                Email = command.Email,
                Roles = (await _rolesRepository.GetByIdesAsync(roles))?.ToList(),
                FirstName = command.FirstName,
                LastName = command.LastName,
                AppliedPromocodesCount = settings.Value.DefaultAppliedPromocodesCount
            };

            var result = await _employeeRepository.AddAsync(employee);

            if (result is null) 
                return BadRequest(new ErrorRequestResult("Не удалось сохранить пользователя"));

            return CreateEmployeeResponse(result);
        }

        /// <summary>
        /// Обновить инфонмацию о пользователе
        /// </summary>
        /// <param name="command"><see cref="UpdateEmployeeCommand">DTO</see> обновления сотрудника</param>
        /// <param name="validator">From Services</param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateEmployeeAsync(
            [FromBody] UpdateEmployeeCommand command, 
            [FromServices] IValidator<UpdateEmployeeCommand> validator)
        {
            var validationResult = await validator.ValidateAsync(command);
            if (!validationResult.IsValid) 
                return BadRequest(new ErrorRequestResult(validationResult
                    .Errors
                    .Select(x => x.ErrorMessage)));

            var employee = await _employeeRepository.GetByIdAsync(command.Id!.Value);
            if (employee is null)
                return BadRequest(new ErrorRequestResult($"Не удалось найти пользователя пользователя с id: {command.Id}"));

            employee.Email = command.Email;
            employee.FirstName = command.FirstName;
            employee.LastName = command.LastName;
            employee.AppliedPromocodesCount = command.AppliedPromocodesCount;

            var result = await _employeeRepository.UpdateAsync(employee);
            
            return result 
                ? (IActionResult)Ok()
                : BadRequest(new ErrorRequestResult("Не удалось обновить пользователя"));
        }

        /// <summary>
        /// Удалить пользователя по id
        /// </summary>
        /// <param name="id">Id пользователя</param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteEmployeeAsync(Guid id)
        {
            var result = await _employeeRepository.RemoveAsync(id);
            return result 
                ? (IActionResult)Ok()
                : BadRequest(new ErrorRequestResult("Не удалось обновить пользователя"));
        }
    }
}