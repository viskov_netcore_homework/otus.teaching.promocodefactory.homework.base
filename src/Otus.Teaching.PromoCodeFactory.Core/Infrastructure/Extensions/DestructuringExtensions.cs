﻿namespace Otus.Teaching.PromoCodeFactory.Core.Infrastructure.Extensions
{
    public static class DestructuringExtensions
    {
        public static void Deconstruct<T>(this T[] items, out T t0)
        {
            t0 = items.Length > 0 ? items[0] : default;
        }

        public static void Deconstruct<T>(this T[] items, out T t0, out T t1)
        {
            t0 = items.Length > 0 ? items[0] : default;
            t1 = items.Length > 1 ? items[1] : default;
        }
        
        public static void Deconstruct<T>(this T[] items, out T t0, out T t1, out T t2, out T t3)
        {
            t0 = items.Length > 0 ? items[0] : default;
            t1 = items.Length > 1 ? items[1] : default;
            t2 = items.Length > 2 ? items[2] : default;
            t3 = items.Length > 3 ? items[3] : default;
        }
    }
}