﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected static ConcurrentDictionary<Guid, T> Data;

        public InMemoryRepository(IEnumerable<T> data)
        {
            if (Data is null)
            {
                Interlocked.CompareExchange(ref Data, new ConcurrentDictionary<Guid, T>(data.ToDictionary(x => x.Id)), null); //TODO Loop with cancellation token?
            }
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data.Values.AsEnumerable());
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            Data.TryGetValue(id, out var result);
            return Task.FromResult(result);
        }

        public Task<IEnumerable<T>> GetByIdesAsync(IEnumerable<Guid> ides)
        {
            var keys = ides as Guid[] ?? ides?.ToArray() ?? new Guid[0];
            var result = Data
                .Where(x => keys.Contains(x.Key))
                .Select(x => x.Value);
            return Task.FromResult(result);
        }

        public Task<T> AddAsync(T entity)
        {
            entity.Id = Guid.NewGuid();
            return Data.TryAdd(entity.Id, entity) 
                ? Task.FromResult(entity) 
                : Task.FromResult<T>(default);
        }

        public Task<bool> UpdateAsync(T entity)
        {
            if (!Data.ContainsKey(entity.Id)) return Task.FromResult(false);
            Data[entity.Id] = entity;
            return Task.FromResult(true);
        }

        public Task<bool> RemoveAsync(Guid id)
        {
            return Task.FromResult(Data.ContainsKey(id) && Data.TryRemove(id, out _));
        }

        public Task<IEnumerable<T>> Where(Expression<Func<T, bool>> filter)
        {
            var compiledFilter = filter.Compile();        //TODO Cache it
            return Task.FromResult(Data.Values.Where(compiledFilter));
        }
    }
}